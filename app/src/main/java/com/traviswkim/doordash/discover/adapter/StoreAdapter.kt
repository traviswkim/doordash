package com.traviswkim.doordash.discover.adapter

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.traviswkim.doordash.data.Store
import com.traviswkim.doordash.databinding.RowStoreBinding

class StoreAdapter(private val storeList: ArrayList<Store>, private val clickListener: OnItemClickListener): RecyclerView.Adapter<StoreAdapter.StoreViewHolder>(){

    interface OnItemClickListener{
        fun itemClicked(store: Store)
    }

    inner class StoreViewHolder(private val binding: RowStoreBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(store: Store){
            binding.apply {
                name.text = store.name
                description.text = store.description
                Glide.with(binding.root)
                    .load(store.coverImgUrl)
                    .placeholder(ColorDrawable(Color.GRAY))
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .into(coverImg)
                val asapAvgMinutes = store.status.asapMinutesRange.average().toInt()
                if(store.status.unavailableReason.isNullOrEmpty()){
                    status.text = "$asapAvgMinutes Mins"
                }else{
                    status.text = store.status.unavailableReason
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreViewHolder {
        val binding = RowStoreBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return StoreViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return storeList.size
    }

    override fun onBindViewHolder(holder: StoreViewHolder, position: Int) {
        holder.bind(storeList[position])
        holder.itemView.setOnClickListener {
            clickListener.itemClicked(storeList[position])
        }
    }

    fun addStores(stores: List<Store>){
        this.storeList.apply {
            clear()
            addAll(stores)
        }
    }
}