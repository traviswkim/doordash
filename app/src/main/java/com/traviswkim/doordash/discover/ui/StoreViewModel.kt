package com.traviswkim.doordash.discover.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.traviswkim.doordash.discover.repository.StoreRepository
import com.traviswkim.doordash.retrofit.RetrofitClient
import kotlinx.coroutines.Dispatchers

class StoreViewModel(private val repository: StoreRepository): ViewModel() {
    fun getStoreDetail(storeId: Int) = liveData(Dispatchers.IO){
        emit(repository.getStore(storeId))
    }
    fun getStoreFeed(lat: String, lng: String, offset: Int, limit: Int) = liveData(Dispatchers.IO){
        emit(repository.getStoreFeed(lat, lng, offset, limit))
    }
}

class ViewModelFactory(private val retrofit: RetrofitClient): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(StoreViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return StoreViewModel(StoreRepository(retrofit)) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}