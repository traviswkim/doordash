package com.traviswkim.doordash.discover.ui

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.traviswkim.doordash.R
import com.traviswkim.doordash.databinding.FragmentStoreDetailBinding

private const val ARG_STORE_ID = "paramStoreId"

class StoreDetailsFragment : Fragment() {
    private lateinit var storeViewModel: StoreViewModel
    private var paramStoreId: Int? = null
    private var _binding: FragmentStoreDetailBinding ?= null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            paramStoreId = it.getInt(ARG_STORE_ID)
        }
        setupViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentStoreDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setBackNavButton()
        setupObserver()
    }

    private fun setBackNavButton(){
        (activity as? AppCompatActivity)?.apply{
            this.supportActionBar?.apply {
                setDisplayHomeAsUpEnabled(true)
            }
            binding.collapsingToolbar.toolbar.setNavigationIcon(R.drawable.ic_back_arrow)
            this.setSupportActionBar(binding.collapsingToolbar.toolbar)
        }
    }

    private fun setupViewModel(){
        when(activity){
            is DiscoverActivity -> {
                storeViewModel = (activity as DiscoverActivity).obtainStoreViewModel()
            }
        }
    }

    private fun setupObserver(){
        if(this::storeViewModel.isInitialized){
            storeViewModel.getStoreDetail(paramStoreId!!).observe(viewLifecycleOwner, Observer {
                it?.let{ store ->
                    binding.extraInfo.text = getString(R.string.store_extra_info, store.statusType, store.averageRating, store.numberOfRatings)
                    binding.description.text = store.tags.joinToString(", ")
                    val headerImg = if(store.headerImgUrl.isNullOrEmpty()) store.coverImgUrl else store.headerImgUrl
                    Glide.with(requireContext())
                        .load(headerImg)
                        .placeholder(ColorDrawable(Color.GRAY))
                        .into(binding.collapsingToolbar.collapsingCoverImage)
                    binding.collapsingToolbar.collapsingToolbarLayout.title = store.name
                }
            })
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(paramStoreId: Int) =
            StoreDetailsFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_STORE_ID, paramStoreId)
                }
            }
    }
}