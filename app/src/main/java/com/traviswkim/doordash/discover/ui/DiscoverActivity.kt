package com.traviswkim.doordash.discover.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import com.traviswkim.doordash.R
import com.traviswkim.doordash.databinding.ActivityDiscoverBinding
import com.traviswkim.doordash.retrofit.RetrofitClient

class DiscoverActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDiscoverBinding

    fun obtainStoreViewModel() = _storeViewModel
    private val _storeViewModel: StoreViewModel by lazy {
        ViewModelProvider(this, ViewModelFactory(RetrofitClient)).get(StoreViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDiscoverBinding.inflate(layoutInflater)
        setContentView(binding.root)

        loadStoreList()
    }

    private fun loadStoreList(){
        val fragment = supportFragmentManager.findFragmentByTag(StoreListFragment::class.java.simpleName) ?: StoreListFragment.newInstance()
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, fragment)
            .addToBackStack(StoreListFragment::class.java.simpleName)
            .commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            this.onBackPressed()
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}