package com.traviswkim.doordash.discover.repository

import com.traviswkim.doordash.retrofit.RetrofitClient

class StoreRepository (private val retrofit: RetrofitClient) {
    suspend fun getStoreFeed(lat: String, lng: String, offset: Int = 0, limit: Int = 50) = retrofit.storeService.getStores(lat, lng, offset, limit)
    suspend fun getStore(storeId: Int) = retrofit.storeService.getStoreDetail(storeId)
}