package com.traviswkim.doordash.discover.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.traviswkim.doordash.R
import com.traviswkim.doordash.data.Store
import com.traviswkim.doordash.databinding.FragmentStoreListBinding
import com.traviswkim.doordash.discover.adapter.StoreAdapter

class StoreListFragment : Fragment() {

    private var _binding: FragmentStoreListBinding ?= null
    private val binding get() = _binding!!
    private val lat: String = "37.422740"
    private val lng: String = "-122.139956"
    private val offset: Int = 0
    private val limit: Int = 50

    private lateinit var storeViewModel: StoreViewModel
    private lateinit var storeAdapter: StoreAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentStoreListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
        setupObserver()
    }

    private fun setupViewModel(){
        when(activity){
            is DiscoverActivity -> {
                storeViewModel = (activity as DiscoverActivity).obtainStoreViewModel()
            }
        }
    }

    private fun setupUI(){
        //Setup toolbar
        binding.titleBar.toolbar.title = getString(R.string.discover)
        //Setup RecyclerView and Adapter
        storeAdapter = StoreAdapter(arrayListOf(), object : StoreAdapter.OnItemClickListener{
            override fun itemClicked(store: Store) {
                (activity as? DiscoverActivity)?.supportFragmentManager
                    ?.beginTransaction()
                    ?.replace(R.id.fragmentContainer, StoreDetailsFragment.newInstance(store.id))
                    ?.addToBackStack(StoreDetailsFragment::class.java.simpleName)
                    ?.commit()
            }
        })

        binding.storeRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(context, (layoutManager as LinearLayoutManager).orientation))
            this.adapter = storeAdapter
        }
    }

    private fun setupObserver(){
        if(this::storeViewModel.isInitialized){
            storeViewModel.getStoreFeed(lat, lng, offset, limit).observe(viewLifecycleOwner, Observer {
                it?.let{ storeFeed ->
                    storeAdapter.apply {
                        addStores(storeFeed.stores)
                        notifyDataSetChanged()
                        binding.progressBar.visibility = View.GONE
                    }
                }
            })
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        @JvmStatic
        fun newInstance() = StoreListFragment()
    }
}