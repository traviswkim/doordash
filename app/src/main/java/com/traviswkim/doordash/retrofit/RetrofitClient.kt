package com.traviswkim.doordash.retrofit

import com.traviswkim.doordash.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {
    private const val BASE_URL = "https://api.doordash.com/"
    private fun getInstance(): Retrofit {
        val loggingInterceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())

        return if(BuildConfig.DEBUG){
            retrofit.client(OkHttpClient.Builder().addInterceptor(loggingInterceptor).build()).build()
        }else{
            retrofit.build()
        }
    }

    val storeService: StoreService = getInstance().create(StoreService::class.java)
}