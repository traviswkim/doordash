package com.traviswkim.doordash.retrofit

import com.traviswkim.doordash.data.StoreDetail
import com.traviswkim.doordash.data.StoreFeed
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface StoreService {
    @GET("/v1/store_feed/")
    suspend fun getStores(@Query("lat") lat: String,
                          @Query("lng") lng: String,
                          @Query("offset") offset: Int,
                          @Query("limit") limit: Int
    ): StoreFeed

    @GET("/v2/restaurant/{restaurant_id}/")
    suspend fun getStoreDetail(
        @Path("restaurant_id") restaurantId: Int
    ): StoreDetail
}