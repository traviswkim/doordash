package com.traviswkim.doordash.data

import com.google.gson.annotations.SerializedName

data class StoreDetail(
    @SerializedName("id")
    val id: Int,
    @SerializedName("business_id")
    val businessId: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("cover_img_url")
    val coverImgUrl: String,
    @SerializedName("header_image_url")
    val headerImgUrl: String?,
    @SerializedName("average_rating")
    val averageRating: Double,
    @SerializedName("number_of_ratings")
    val numberOfRatings: Int,
    @SerializedName("status_type")
    val statusType: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("tags")
    val tags: List<String>
)