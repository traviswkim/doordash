package com.traviswkim.doordash.data

import com.google.gson.annotations.SerializedName

data class StoreFeed(
    @SerializedName("num_results")
    val numResult: Int,
    @SerializedName("is_first_time_user")
    val isFirstTimeUser: Boolean,
    @SerializedName("next_offset")
    val nextOffset: Int,
    @SerializedName("stores")
    val stores: List<Store>
)