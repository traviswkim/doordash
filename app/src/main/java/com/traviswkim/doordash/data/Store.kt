package com.traviswkim.doordash.data

import com.google.gson.annotations.SerializedName

data class Store(
    @SerializedName("id")
    val id: Int,
    @SerializedName("business_id")
    val businessId: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("cover_img_url")
    val coverImgUrl: String,
    @SerializedName("distance_from_consumer")
    val distanceFromConsumer: Double,
    @SerializedName("status")
    val status: Status
)