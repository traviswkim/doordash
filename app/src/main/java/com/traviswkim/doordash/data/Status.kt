package com.traviswkim.doordash.data

import com.google.gson.annotations.SerializedName

data class Status(
    @SerializedName("unavailable_reason")
    val unavailableReason: String ?= null,
    @SerializedName("pickup_available")
    val pickupAvailable: Boolean,
    @SerializedName("asap_available")
    val asapAvailable: Boolean,
    @SerializedName("scheduled_available")
    val scheduledAvailable: Boolean,
    @SerializedName("asap_minutes_range")
    val asapMinutesRange: List<Int>
)